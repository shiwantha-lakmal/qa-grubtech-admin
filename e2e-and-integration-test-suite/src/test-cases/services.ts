
describe('sample web api level test case', () => {
  it('should pass', () => {
    cy.visit("https://example.cypress.io/commands/network-requests")
    let message = 'whoa, this comment does not exist'
    cy.server()

    // Listen to GET to comments/1
    cy.route('GET', 'comments/*').as('getComment')
    // the button is clicked in scripts.js
    cy.get('.network-btn').click()
    // https://on.cypress.io/wait
    cy.wait('@getComment').its('status').should('eq', 200)
  });
});
