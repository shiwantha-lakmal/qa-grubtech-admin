export class GrubTechAdminLoginPage{

    private txtFldGlobalSearch  : any = cy.get('.gLFyf');

    constructor(){

    }

   /**
   * Steps : Set Search Keyword
   * @param name
   * @returns {GrubTechAdminLoginPage}
   */
  step_EnterSerchKeyword(name: string) {
    this.txtFldGlobalSearch.type(name+'{enter}');
    return new GrubTechAdminLoginPage();
  }
}